#define capteur_hum1  A1
#define pompe1 5
#define seconde 1000
#define minute 60000
#define heure  3600000
#define TWELVE_HRS 43200000UL

// capteur 1
int valeur_capt1 = 0;
int controle_arrosage_max = 10;

void setup() {
  Serial.begin(9600);
  pinMode(capteur_hum1, INPUT);
  pinMode(pompe1, OUTPUT);
  digitalWrite(pompe1, HIGH); // Ensure the pump is initially off
}

void loop() {
  Serial.println("Debut de boucle");
  int jai_arrose = 0; // Variable check si ça arrose
  int valeur_moy_capt1 = 0; // Variable pour faire la moyenne des mesures 
  int lecture1 = 0; // Variable du nombre de lecture du capteur 
  int valeur1 = 0; // Variable de lecture de l'humidité
  
  // Fail-safe si on arrose trop  
  int controle_error = 0;
  while (controle_error < 3) {

    // On check 10 valeurs toutes les 2 secondes 
    while (lecture1 < 10) {
      valeur1 = analogRead(capteur_hum1);
      Serial.print("Read value: ");
      Serial.println(valeur1);
      // Check si les valeurs sont cohérentes
      if (valeur1 > 200 && valeur1 < 500) {
        valeur_moy_capt1 += valeur1;
        lecture1++;
      }
      delay(2 * seconde);
     
    }

    // Valeur d'humidité moyenne 
    valeur_moy_capt1 /= lecture1;
    Serial.print("Average moisture value: ");
    Serial.println(valeur_moy_capt1);

      // On arrose si la valeur du capteur est basse 
      if (valeur_moy_capt1 > 320) {
        Serial.println("J'arrose");
        digitalWrite(pompe1, LOW);
        delay(5 * seconde); // arrose moi ça.
        digitalWrite(pompe1, HIGH);
      }

      delay(10 * minute);
      controle_error++;
      jai_arrose = 1;
      valeur_moy_capt1 = 0;
      lecture1 = 0;
  }
  Serial.println("Fini arrosage");
  delay(10 * minute);  
  // Check si ça arrose, tout court
  if (jai_arrose == 1) {
    Serial.println("Check si ça arrose, tout court");
    valeur1 = analogRead(capteur_hum1);
    Serial.print("Valeur de check:");
    Serial.println(valeur1);
    int delta_capt_max = 50;
    int delta_max1 = valeur_moy_capt1 - valeur1;

    Serial.print("Check de delta: ");
    Serial.println(delta_max1);

    jai_arrose = 0;
    // si l'humidité n'est pas baissée on arrête, cela veut que soit le capteur ne fonctionne pas. Ou la pompe ne fonctionne pas
    if (delta_max1 < delta_capt_max) {
      Serial.println("Delta MAX if statement");
      digitalWrite(pompe1, HIGH); // Ensure the pump is off
      while(1){
        Serial.println("WHILE EXIT LOOP");
        //empty                                                                      
      }
    }
  }
  
  delayHours(12);
  
}

void delayHours(unsigned int hours) {
  // Convert hours to milliseconds
  unsigned long ms = hours * 3600000UL; // 1 hour = 3600000 milliseconds
  unsigned long start = millis();
  while (millis() - start < ms) {
    // Use smaller delays to avoid overflow issues
    delay(60000); // Delay for 1 minute at a time (60000 milliseconds)
  }
}
